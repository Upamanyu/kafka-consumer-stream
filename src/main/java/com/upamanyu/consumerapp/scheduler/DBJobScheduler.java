package com.upamanyu.consumerapp.scheduler;

import com.upamanyu.consumerapp.DTO.LocationDTO;
import com.upamanyu.consumerapp.entity.Location;
import com.upamanyu.consumerapp.repository.LocationRepository;
import com.upamanyu.consumerapp.service.impl.RecordStorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DBJobScheduler {

    @Autowired
    private LocationRepository locationRepository;

    @Scheduled(fixedRate = 1000*60*5)
    public void saveToDB() {
        // take the latest record from kafka consumer blocking queue
        log.info("Running db call...");
        Map<String,LocationDTO> latestRecords = RecordStorageService.getLatestRecords();

       List<Location> newestLocations = latestRecords.values().stream()
                       .map(locationDTO -> new Location(locationDTO.getName(),
                               locationDTO.getLatitude(), locationDTO.getLongitude()))
                               .collect(Collectors.toList());

       locationRepository.saveAll(newestLocations);
        // log the record that is saved

    }

}
