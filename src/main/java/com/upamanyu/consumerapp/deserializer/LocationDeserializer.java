package com.upamanyu.consumerapp.deserializer;

import com.upamanyu.consumerapp.DTO.LocationDTO;
import org.apache.kafka.common.serialization.Deserializer;

import java.nio.charset.StandardCharsets;

public class LocationDeserializer implements Deserializer<LocationDTO> {
    @Override
    public LocationDTO deserialize(String topic, byte[] byteData) {
        String[] fields = new String(byteData, StandardCharsets.UTF_8).split("\\|");
        String name = fields[0];
        double lat = Double.parseDouble(fields[1]);
        double lon = Double.parseDouble(fields[2]);
        return new LocationDTO(name,lat,lon);
    }
}
