package com.upamanyu.consumerapp.service.impl;

import com.upamanyu.consumerapp.DTO.LocationDTO;
import com.upamanyu.consumerapp.config.KafkaConsumerConfig;
import com.upamanyu.consumerapp.service.KafkaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaServiceImpl implements KafkaService {

    @Override
    @KafkaListener(topics = "${kafka.topic}", groupId = "group-1")
    public void locationConsumer(LocationDTO location) {
        // update the latest record
        // update the map with latest value
        RecordStorageService.updateLatestRecords(location.getName(), location);

    }
}
