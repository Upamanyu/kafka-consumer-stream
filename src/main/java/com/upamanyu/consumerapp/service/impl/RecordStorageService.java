package com.upamanyu.consumerapp.service.impl;

import com.upamanyu.consumerapp.DTO.LocationDTO;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class RecordStorageService  {

    private static final Map<String, LocationDTO> latestRecords = new ConcurrentHashMap<>();

    // update from kafka listener
    public static void updateLatestRecords(String name, LocationDTO location) {
         latestRecords.put(name,location);
    }

    public static Map<String, LocationDTO> getLatestRecords() {
        return latestRecords;
    }
}
