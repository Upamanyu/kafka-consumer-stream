package com.upamanyu.consumerapp.service;

import com.upamanyu.consumerapp.DTO.LocationDTO;


public interface KafkaService {

    public void locationConsumer(LocationDTO locationDTO);
}
