package com.upamanyu.consumerapp.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LocationDTO implements Serializable {

    private String name;
    private Double latitude;
    private Double longitude;

}
